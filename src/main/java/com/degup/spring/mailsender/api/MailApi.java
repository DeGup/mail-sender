package com.degup.spring.mailsender.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MailApi {

    private final JavaMailSender emailSender;
    private final String to;

    @Autowired
    public MailApi(@Value("${spring.mail.username}") String to, JavaMailSender emailSender) {
        this.to = to;
        this.emailSender = emailSender;
    }

    /**
     * Je endpoint relatief vanaf de context-path in de application.yml. In dit geval dus localhost:9990/mail-sender/send
     * Postmapping, dus een POST is nodig op dit endpoint.
     *
     * @param body De body is de inhoud van je mail
     * @param subject Uiteraard je subject, moet in een header met naam 'subject'
     * @return 202 als alles goed ging.
     */
    @PostMapping("/send")
    public ResponseEntity sendMail(@RequestBody String body,
                                   @RequestHeader(value = "subject") String subject) {

        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(to);
        message.setFrom(to);// from moet gelijk zijn aan to, anders gaat micro$oft over zijn nek.
        message.setSubject(subject);
        message.setText(body); // content in plain text
        emailSender.send(message);

        // geeft een nette HTTP 202 terug als de mail verstuurd is.
        return ResponseEntity.accepted().build();
    }

}
