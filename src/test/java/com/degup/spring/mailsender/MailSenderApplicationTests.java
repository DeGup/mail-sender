package com.degup.spring.mailsender;

import com.degup.spring.mailsender.api.MailApi;
import com.dumbster.smtp.SimpleSmtpServer;
import com.dumbster.smtp.SmtpMessage;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MailSenderApplicationTests {


    @Autowired
    MailApi mailApi;

    /**
     * Simpele test om even te controleren of de mail werkt. Gebruikt de settings in test/resources, dus niet je echte settings.
     * @throws Exception
     */
    @Test
    public void sendMail() throws Exception {
        // given
        SimpleSmtpServer dumpster = SimpleSmtpServer.start(25);

        // when
        mailApi.sendMail("hi", "Subject");

        // then
        dumpster.stop();

        List<SmtpMessage> receivedEmails = dumpster.getReceivedEmails();

        assertThat(receivedEmails.size(), is(1));
        SmtpMessage message = receivedEmails.get(0);
        assertThat(message.getBody(), containsString("hi"));
        assertThat(message.getHeaderValue("To"), is("test"));
        assertThat(message.getHeaderValue("Subject"), is("Subject"));
    }

}
